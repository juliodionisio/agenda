package br.com.itau;

import java.util.Scanner;

public class Menu {
    Scanner menu = new Scanner(System.in);
    String opcaoMenu = new String();
    boolean rodando = true;
    Agenda agenda = new Agenda();

    public void menuIncial(){
        while(rodando){
            Scanner scanner = new Scanner(System.in);
            System.out.println("Qual o nome do contato?");
            String nome = scanner.next();

            System.out.println("Qual o email do contato?");
            String email = scanner.next();

            Contato novoContato = new Contato(nome, email);
            agenda.incluirContato(novoContato);
            System.out.println("Lista de contatos:");
            agenda.listarContatos();
            System.out.println("*****");

            exibirMenu();



        }
    }
    public void exibirMenu(){
        System.out.println("Escolha umas das opções abaixo: \n 1 - Cadastrar novo contato \n 2 - Remover contato por e-mail \n 3 - Remover contato por id \n 4 - Imprimir os dados de um contato por e-mail \n 5 - Imprimir os dados de um contato pelo id");

        opcaoMenu = menu.next();

        switch (opcaoMenu){
            case"1":
                break;
            case"2":
                agenda.removerContatoPorEmail(obterEmail()); exibirMenu();
            case"3":
                agenda.removerContatoPorId(obterId()); exibirMenu();
            case"4":
                agenda.imprimirContato(agenda.buscarContatoPorEmail(obterEmail())); exibirMenu();
            case"5":
                agenda.imprimirContato(agenda.buscarContatoPorId(obterId())); exibirMenu();

        }

    }

    private String obterEmail(){
        System.out.println("Insira o e-mail desejado:");
        String email = menu.next();
        return email;
    }

    private int obterId(){
        System.out.println("Insira o ID desejado:");
        int id = menu.nextInt();
        return id;
    }
}
