package br.com.itau;

public class Contato {
    private String nome;
    private String email;
    private int id;

    public Contato(String nome,String email,int id){
        this.nome = nome;
        this.email = email;
        this.id = id;
    }

    public Contato(String nome,String email){
        this.nome = nome;
        this.email = email;
    }

    public Contato(){

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
