package br.com.itau;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import org.w3c.dom.ls.LSOutput;

import java.util.Iterator;
import java.util.LinkedList;

public class Agenda {

    private LinkedList<Contato> agenda = new LinkedList<Contato>();
    Iterator<Contato> buscador = agenda.iterator();
    private int quantidadeLista = 0;
    Contato contatoAuxiliar = new Contato();

     public void Agenda(){}


    public LinkedList<Contato> getAgenda() {
        return agenda;
    }

    public void setAgenda(LinkedList<Contato> agenda) {
        this.agenda = agenda;
    }


    public void incluirContato(Contato contato) {
        contato.setId(quantidadeLista + 1);
        agenda.add(contato);
        quantidadeLista ++;
    }

    public Contato buscarContatoPorNome(String nome){
        //Contato contato = buscador.next();
        for(int n = 0; n < agenda.size(); n++){
            if (agenda.get(n).getNome().equals(nome)) return agenda.get(n);
        }
        return null;
    }

    public Contato buscarContatoPorEmail(String email){
        //Contato contato = buscador.next();
        for(int n = 0; n < agenda.size(); n++){
            if (agenda.get(n).getEmail().equals(email)) return agenda.get(n);
        }
        return null;
    }
    public Contato buscarContatoPorId(int id){
        //Contato contato = buscador.next();
        for(int n = 0; n < agenda.size(); n++){
            if (agenda.get(n).getId() == id) return agenda.get(n);
        }
        return null;
    }

    public void listarContatos(){
        //Contato contato = buscador.next();
        for(int n = 0; n < agenda.size(); n++) {
            imprimirContato(agenda.get(n));
            System.out.println("*****");
        }
    }

    public void imprimirContato(Contato contato){
        System.out.println("Nome: "+ contato.getNome() + "\n Email: " + contato.getEmail() + "\n Número: " + contato.getId());
    }

    public void removerContatoPorEmail(String email){
         Contato contatoRemover = buscarContatoPorEmail(email);
         if(agenda.remove(contatoRemover)) System.out.println("Contato removido com sucesso!");
         else System.out.println("Contato não encontrado!");
    }

    public void removerContatoPorId(int id){
        Contato contatoRemover = buscarContatoPorId(id);
        if(agenda.remove(contatoRemover)) System.out.println("Contato removido com sucesso!");
        else System.out.println("Contato não encontrado!");
        listarContatos();
    }

    //public void removerContatoPorEmail()


}
